var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Hello this page will be the Landing Page see you soon!' });
});

module.exports = router;
